clear all
clc
close all
format long

ranges_n   = [1,2,4,8,16,32,64];
pol_degs   = [2,3,4];
max_ranges = [64,32,8];

[x,i_max] = size(pol_degs  );
[x,k_max] = size(ranges_n  );
linspace_points=2000;
fid = fopen('sqrt.txt','wt');
fprintf(fid, '------------- SQRT ------------- \n');
fprintf(fid, 'pol_deg , N , max abs rel error \n');
for i=1:i_max
    max_range = max_ranges(i);
    pol_deg = pol_degs(i);
    k=1;
    %disp([i max_ranges(k) max_range])
    while ranges_n(k)<=max_range
        %disp([pol_deg,ranges_n(k)])
        maxABSrelError=sqrSolEval(pol_deg,ranges_n(k),linspace_points);
        fprintf(fid, '%u & %u & %.4e \\\\ \n',pol_deg,ranges_n(k), maxABSrelError);
        if(k+1>k_max)
            k=k_max;
            break;
        else
            k=k+1;
        end
    end
end
fclose(fid);

