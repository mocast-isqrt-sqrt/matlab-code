 
# A  novel  approximation  scheme  for  floating-point square  root  and  inverse  square  root  for  FPGAs


- To generate the Chebyshev approximation for sqrt, use cheby_sqrt.m.
- To generate the Chebyshev approximation for isqrt, use cheby_isqrt.m.

Use variables pol_deg and intervals_n to modify the approximating polynomial degree and intervals numbers, respectively.
Use scripts  table_cheby_isqrt.m and  table_cheby_sqrt.m to replicate the results of TABLE I. The results of these scripts are 
displayed in two text file isqrt.txt and sqrt.txt