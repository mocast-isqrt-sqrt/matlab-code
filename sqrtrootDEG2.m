function F = sqrtrootDEG2(x,xRange)
p0=x(1);
p1=x(2);
p2=x(3);
L =x(4);
x1=x(5);
x2=x(6);

x_start = xRange(1);
x_stop  = xRange(2);

F(1) = p2 * x_start ^ 2 + p1 * x_start + p0 - sqrt(x_start) - L;
F(2) = p2 * x1 ^ 2 + p1 * x1 + p0 - sqrt(x1) + L;
F(3) = p2 * x2 ^ 2 + p1 * x2 + p0 - sqrt(x2) - L;
F(4) = p2 * x_stop ^ 2 + p1 * x_stop + p0 - sqrt(x_stop) + L;
F(5) = (2 * p2 * x1) + p1 - (x1 ^ (-0.1e1 / 0.2e1)) / 0.2e1;
F(6) = (2 * p2 * x2) + p1 - (x2 ^ (-0.1e1 / 0.2e1)) / 0.2e1;
 
end
