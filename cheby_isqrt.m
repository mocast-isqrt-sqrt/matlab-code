clear all
clc
close all
format long

pol_deg = 2;
intervals_n = 2;
linspace_points =2000;
[init_solutions,intervals] = isqrt_init_solutions(intervals_n,pol_deg,linspace_points);
sytem_sols =[];
max_errors=[];
function_approx =[];
function_exact  =[];
x_range = [];
for i=1:intervals_n
    init_guess = init_solutions(i,:);  
    xRange = intervals(i,:);
   
    switch pol_deg
    case 2
        fun = @(x) isqrtrootDEG2(x, xRange);
        
    case 3
        fun = @(x) isqrtrootDEG3(x, xRange);
        
    otherwise
        fun = @(x) isqrtrootDEG4(x, xRange);
        
    end
    
    options  = optimoptions('fsolve', 'Algorithm','levenberg-marquardt','FunctionTolerance',1e-120,'StepTolerance',1e-32,'MaxFunctionEvaluations',1e4,'MaxIterations',5*1e3);
    x = fsolve(fun,init_guess ,options);
    sytem_sols =[sytem_sols;x];
    xData = linspace(xRange(1),xRange(2),linspace_points);
    x_range=[x_range,xData];
	p=flip(x(1:pol_deg+1)); %[x^pol_deg , x^(pol_deg-1) , ...]
    error_data = arrayfun(@(a)  polyval(p,a) -  1/sqrt(a), xData);
    max_errors=[max_errors , max(abs(error_data))];
	function_approx =[function_approx, arrayfun(@(a) polyval(p,a) , xData)];
	function_exact  = [function_exact, arrayfun(@(a)  1/sqrt(a)  , xData)];
end

 
 
figure
plot(x_range,function_approx-function_exact,'LineWidth',2,'color','black')
xlabel 'Mantissa'
ylabel 'Error'
grid on

isqrt_perc_error=100*(function_approx-function_exact)./(function_exact);
figure
plot(x_range, isqrt_perc_error,'LineWidth',2,'color','black')
xlabel 'Mantissa'
ylabel 'Percent Error'
grid on



