function [init_solutions,intervals] = sqrt_init_solutions(intervals_n,pol_deg,linspacepoints)
	delta = 2^(-log2(intervals_n));
	syms a
	init_solutions=[];
	intervals =[];


	x_start = 1;
	for i=1:intervals_n 
		x_stop =x_start+delta;
		intervals =[intervals;[x_start,x_stop]];
		x = linspace(x_start,x_stop,linspacepoints);
		y_data = arrayfun(@(a) sqrt(a), x);
		p = polyfit(x,y_data,pol_deg);
		
		error_data = arrayfun(@(a) polyval(p,a) - sqrt(a), x);
		L_init = abs(max(error_data));
		TF_min = islocalmin(error_data);
		TF_max = islocalmax(error_data);
		
		x_minima = x(TF_min);
		x_maxima = x(TF_max);
        
        [i_minima, k_minima]=size(x_minima);
        [i_maxima, k_maxima]=size(x_maxima);
        
%         if(mod(pol_deg,2)~=0)
%             k_minima_max = (pol_deg-1)/2 +1
%             k_maxima_max = (pol_deg-1)/2;
%         else
%             k_minima_max = (pol_deg)/2;
%             k_maxima_max = (pol_deg)/2;
%         end
%         if(k_minima>k_minima_max)
%             x_minima=x_minima(1:k_minima_max);
%         end
%         if(k_maxima>k_maxima_max)
%             x_maxima=x_maxima(1:k_maxima_max);
%         end
%         
%         [i_minima, k_minima]=size(x_minima);
%         [i_maxima, k_maxima]=size(x_maxima);
%   
        if(k_minima==k_maxima)
		extrema = [x_minima; x_maxima];
        %extrema = [x_maxima; x_minima];
		extrema=extrema(:)'; % https://it.mathworks.com/matlabcentral/answers/157640-how-can-i-combine-two-vector-into-one-vector-by-the-follwoing-pattern-and-not-use-for-loop-whi
        elseif(k_maxima<k_minima)
            x_minima_reduced = x_minima(1:k_minima-1);
            extrema = [x_minima_reduced; x_maxima];
            extrema=extrema(:)';
            extrema=[extrema x_minima(k_minima)];
        else
            x_maxima_reduced = x_maxima(1:k_maxima-1);
            extrema = [x_maxima_reduced; x_minima];
            extrema=extrema(:)';
            extrema=[extrema x_maxima(k_maxima)];
            L_init = -L_init ;
        end
        
        extrema= linspace(x_start,x_stop,pol_deg);
		init_sol = [ flip(p),L_init ,extrema];
        %% check inti_sol size
         %k_sol_max=2*pol_deg+2;
         
        
        %% Add init solution
		init_solutions=[init_solutions;init_sol];
		x_start =x_stop;
        
	end

end









