function F = isqrtrootDEG3(x,xRange)
p0=x(1);
p1=x(2);
p2=x(3);
p3=x(4);
L =x(5);
x1=x(6);
x2=x(7);
x3=x(8);

x_start = xRange(1);
x_stop  = xRange(2);



F(1) = p3 * x_start ^ 3 + p2 * x_start ^ 2 + p1 * x_start + p0 - x_start ^ (-0.1e1 / 0.2e1) - L;
F(2) = p3 * x1 ^ 3 + p2 * x1 ^ 2 + p1 * x1 + p0 - x1 ^ (-0.1e1 / 0.2e1) + L;
F(3) = p3 * x2 ^ 3 + p2 * x2 ^ 2 + p1 * x2 + p0 - x2 ^ (-0.1e1 / 0.2e1) - L;
F(4) = p3 * x3 ^ 3 + p2 * x3 ^ 2 + p1 * x3 + p0 - x3 ^ (-0.1e1 / 0.2e1) + L;
F(5) = p3 * x_stop ^ 3 + p2 * x_stop ^ 2 + p1 * x_stop + p0 - x_stop ^ (-0.1e1 / 0.2e1) - L;
F(6) = (3 * p3 * x1 ^ 2) + (2 * p2 * x1) + p1 + (x1 ^ (-0.3e1 / 0.2e1)) / 0.2e1;
F(7) = (3 * p3 * x2 ^ 2) + (2 * p2 * x2) + p1 + (x2 ^ (-0.3e1 / 0.2e1)) / 0.2e1;
F(8) = (3 * p3 * x3 ^ 2) + (2 * p2 * x3) + p1 + (x3 ^ (-0.3e1 / 0.2e1)) / 0.2e1;



 
end
